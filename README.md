# solutions

This repository will have my solutions to several different sites including

Nodeschool - http://nodeschool.io

FreeCodeCamp - http://www.freecodecamp.com

Exploit Exercises - https://exploit-exercises.com

Overthewire - http://overthewire.org/wargames/


These sites and others like them are highly recommended for anybody learning or who just wants to challenge their mind and are run by people who give a lot of their time so support them however possible.
