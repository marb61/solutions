These are the solutions to the Nodeschool Async You set of challenges

Level 1 Waterfall -
This is the first challenge and according to the documentation there is a need to write a program that first reads the
contents of a file.The path will be provided as the first command-line argument to your program and the file will 
contain a single URL. Using http.get, create a GET request to this URL and console.log the response body. This is a 
relatively basic challenge to start as there is a lot of code given so here is the solution 

var http = require('http')
var async = require("async");
var fs = require("fs");
var filepath = process.argv[2];

async.waterfall([
    function(cb){
        fs.readFile(filepath, function(err, data){
            if(err){
                return cb(err);
            }else{
                cb(null, data.toString());
            }
        });
    },
    function(urlData, cb){
        var body = '';
        http.get(urlData, function(res){
                res.on('data', function(chunk){
                body += chunk.toString();
            });
                res.on('end', function(){
                cb(null, body);
            });
        }).on('error', function(err) {
          cb(err);
        });
        }
    ], 
    function(err, result){
      if (err) return console.error(err);
      console.log(result);
    }
    );

This has a lot of the boilerplate code that was given used with a little amount of changes as that was all the 
challenge needed.



Level 2 Series Object - 
In this challenge the site says that it is going to be async.series that will be the basis of the challenge and it
discusses the difference between this and the waterfall method used in the previous challenge. In the words of the
site itself regarding the task
Write a program that will receive two URLs as the first and second command-line arguments.
Using http.get, create a GET request to these URLs and pass the response body
to the callback.
Pass in an object of task functions, using the property names requestOne and
requestTwo, to async.series.
console.log the results in the callback for series when all the task functions
have completed.
Like the previous challenge there has been plenty of boilerplate code given with hints so the challenge should be
quick to solve. The solution should look like this

var http = require('http');
var async = require("async");
var firstpath = process.argv[2];
var secondpath = process.argv[3];

async.series({
    requestOne: function(cb){
        http.get(firstpath, function(res){
            var body = '';
            res.on('data', function (chunk) {
                        body += chunk.toString();
                    }).on('end', function () {
                        cb(null, body);
                    });
                }).on('error', function (err){
            return cb(err);
        });
    },
    requestTwo: function(cb){
        http.get(secondpath, function(res){
            var body = '';
            res.on('data', function (chunk) {
                        body += chunk.toString();
                    }).on('end', function () {
                        cb(null, body);
                    });
                }).on('error', function (err) {
            return cb(err);
        });
     }    
    },
    function(err, result){
      if (err) return console.error(err);
      console.log(result);
    }
);

As you can see the previous challenge gave a lot of code that could be reused and from there it was a matter of
changing only a few things using the hints and documentation. The official solution is also well worth a look

var http = require('http')
      , async = require('async');
    
    async.series({
      requestOne: function(done){
        fetchURL(process.argv[2], done);
      },
      requestTwo: function(done){
        fetchURL(process.argv[3], done);
      }
    },
    function(err, result){
      if (err) return console.error(err);
      console.log(result);
    });
    
    function fetchURL(url, done) {
      var body = '';
      http.get(url, function(res){
        res.on('data', function(chunk){
          body += chunk.toString();
        });
    
        res.on('end', function(chunk){
          done(null, body);
        });
      }).on('error', function(e){
        done(e);
      });
    }

They have chosen to do things slightly different by defining a seperate function and then calling it in the series.



Level 3 Each -
In this the third level the challenge is to use async.each. The task itself is this -
Create a program that will receive two URLs as the first and second command-line
arguments.
Then using http.get, create two GET requests to these URLs and console.log
any errors
As usual there is a lot of boilerplate code and hints and this along with experience and code from the previous
examples should provide a solution. This should look like

var http = require('http');
var async = require("async");
var firstUrl = process.argv[2];
var secondUrl = process.argv[3];

async.each([firstUrl, secondUrl], function(item, done){
    http.get(item, function(res){
        res.on('data', function(chunk){
            
        });
        res.on('end', function() {
            done();
        });
        }).on('error', function (err) {
            done(err);
        });
        }, function(err){
            if (err)
            console.log(err);
});

Again there is not that much difference between the official solution and this probably from using the hints
and the boilerplate code.
